//
//  Webservice.swift
//  Shipping Rates
//
//  Created by Ismael AlShabrawy on 9/14/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import Foundation
import Alamofire

//Enum to Network Error
enum NetworkError: Error {
    case decodingError
    case domainError
    case urlError
}

struct Resource<T: Codable> {
    let url: URL
    let endPoint: String
}

class Webservice {
    func load<T>(resource: Resource<T>, method: HTTPMethod,
                 prams: Parameters?,
                 headers: HTTPHeaders?,
                 encoding: URLEncoding, completion: @escaping (Result<T, NetworkError>) -> Void) {
        AF.request(URL(string: "\(resource.url)\(resource.endPoint)")!, method: method, parameters: prams, encoding: encoding, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(_):
                let decoder = JSONDecoder()
                do {
                    let result = try decoder.decode(T.self, from: response.data!)
                    DispatchQueue.main.async {
                        completion(.success(result))
                    }
                } catch {
                    let ss = String(data: response.data!, encoding: .utf8)
                    completion(.failure(.decodingError))
                }
                
            case .failure(_):
                completion(.failure(.decodingError))
          }
        }
    }
}
