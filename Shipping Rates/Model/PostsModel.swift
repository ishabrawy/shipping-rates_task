//
//  PostsModel.swift
//  Shipping Rates
//
//  Created by Ismael AlShabrawy on 9/14/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import Foundation

struct PostModel: Codable {
    var id: String?
    var title: String?
    var description: String?
    var img_url: String?
    var video_url: String?
    var likes_count: Int?
    var selected: Bool? = false
    
    init(id: String, title: String, description: String, img_url: String, video_url: String, likes_count: Int, selected: Bool = false) {
        self.id = id
        self.title = title
        self.description = description
        self.img_url = img_url
        self.video_url = video_url
        self.likes_count = likes_count
        self.selected = selected
    }
    
    init() {
        self.id = nil
        self.title = nil
        self.description = nil
        self.img_url = nil
        self.video_url = nil
        self.likes_count = nil
        self.selected = false
    }
    
    enum CodingKeys: String, CodingKey {

        case id = "id"
        case title = "title"
        case description = "description"
        case img_url = "img_url"
        case video_url = "video_url"
        case likes_count = "likes_count"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        img_url = try values.decodeIfPresent(String.self, forKey: .img_url)
        video_url = try values.decodeIfPresent(String.self, forKey: .video_url)
    }
}
