//
//  ConfigModel.swift
//  Shipping Rates
//
//  Created by Ismael AlShabrawy on 9/14/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import Foundation

struct ConfigModel: Codable {
    let BaseURL: String
}
