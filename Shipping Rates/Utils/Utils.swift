//
//  Utils.swift
//  Shipping Rates
//
//  Created by Ismael AlShabrawy on 9/14/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import Foundation

class Utils {
    init() {
        
    }
    
    //Get Base url from config.plist file
    func parseConfig() -> ConfigModel {
        let url = Bundle.main.url(forResource: "Config", withExtension: "plist")!
        let data = try! Data(contentsOf: url)
        let decoder = PropertyListDecoder()
        return try! decoder.decode(ConfigModel.self, from: data)
    }
    
}
