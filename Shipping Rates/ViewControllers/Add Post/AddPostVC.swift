//
//  AddPostVC.swift
//  Shipping Rates
//
//  Created by Ismael AlShabrawy on 9/15/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay
import RxCocoa

class AddPostVC: UIViewController {
    
    
    //MARK:- Vars
    
    private let viewModel = RootViewModel()
    private let disposeBag = DisposeBag()
    
    private var selectedChracterVariable = BehaviorRelay<PostModel>(value: PostModel())
    var selectedCharacter: Observable<PostModel> {
        return selectedChracterVariable.asObservable()
    }
    
    var postModel = PostModel()
    //MARK:- End Vars
    
    //MARK:- Outlets
    @IBOutlet weak var titleTxt: UITextField!
    @IBOutlet weak var descriptionTxt: UITextField!
    @IBOutlet weak var sendNewPostBtn: UIButton!
    //MARK:- End Outlets
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.title = "Add New Post"
        setup()
    }
    
    func setup() {
        self.titleTxt.becomeFirstResponder()
        
        //check if title Txt if empty or have 3 char to disable new post button
        self.titleTxt.rx.text.map{$0 ?? ""}.bind(to: viewModel.postTitlePublishSubject).disposed(by: disposeBag)
        //check if description Txt if empty or have 5 char to disable new post button
        self.descriptionTxt.rx.text.map{$0 ?? ""}.bind(to: viewModel.postDescriptionPublishSubject).disposed(by: disposeBag)
        
        viewModel.isValid().bind(to: sendNewPostBtn.rx.isEnabled).disposed(by: disposeBag)
        viewModel.isValid().map { $0 ? 1 : 0.1 }.bind(to: sendNewPostBtn.rx.alpha).disposed(by: disposeBag)
        
        //Listen to images api to get one image from list to send it's url to posts list
        self.viewModel.imagesModel
            .subscribe(onNext: { [weak self] images in
                if (images.count > 0) {
                    let index = arc4random_uniform(UInt32(images.count - 1))
                    let img = images[Int(index)]
                    self!.postModel.img_url = img.download_url
                }
            }).disposed(by: disposeBag)
        
        self.viewModel.fetchImages()
        
    }
    
    //Save new post to posts list
    @IBAction func newData(_ sender: UIButton) {
        self.postModel.title = self.titleTxt.text!
        self.postModel.description = self.descriptionTxt.text!
        self.postModel.likes_count = 0
        self.postModel.selected = false
        self.postModel.video_url = ""
        self.postModel.id = "\(self.viewModel.getRandomID(fromNumber: 1000, toNumber: 20000))"
        
        
        let resource = Resource<PostModel>(url: URL(string: Utils().parseConfig().BaseURL)!, endPoint: "posts")
        let prams = ["title":self.postModel.title!,
                     "description":self.postModel.description!,
                     "likes_count":self.postModel.likes_count!,
                     "video_url":self.postModel.video_url!,
                     "img_url":self.postModel.img_url!,
                     "id":self.postModel.id!] as [String : Any]
        
        Webservice().load(resource: resource, method: .post, prams: prams, headers: nil, encoding: .default) { result in
            switch result {
            case .success(let post):
                self.postModel.id = post.id
                self.selectedChracterVariable.accept(self.postModel)
                self.navigationController?.popToRootViewController(animated: true)
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
}
