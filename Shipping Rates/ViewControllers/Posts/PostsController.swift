//
//  PostsController.swift
//  Shipping Rates
//
//  Created by Ismael AlShabrawy on 9/14/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import RxRelay
import RxCocoa

class PostsController: UIViewController {
    
    //MARK:- Vars
    let PostCellIdentifier = "PostCell"
    var viewModel = RootViewModel()
    var page = 1 {
        didSet {
            //Fetch posts data from api
            self.viewModel.fetchData(page: page)
        }
    }
    
    let disposeBag = DisposeBag()
    
    //MARK:- Outlets
    @IBOutlet weak var postsCollection: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set View Controller Title in navigation bar
        self.title = "Posts"
        
        //Register Custom cell
        self.registerCells()
        
        //Listen to changes in posts model to reload posts collection view
        self.viewModel.postsModel
        .subscribe(onNext: { [weak self] posts in
            if (posts != nil && posts!.count > 0) {
                self!.postsCollection.reloadData()
            }
        }).disposed(by: disposeBag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (self.viewModel.postsViewModel.count == 0) {
            self.page = 1
        }
    }
    
    //Register collection view cell
    private func registerCells() {
        let postNibCell = UINib(nibName: PostCellIdentifier, bundle: nil)
        self.postsCollection.register(postNibCell, forCellWithReuseIdentifier: PostCellIdentifier)
    }
    
    
    //Fetch posts data from api
    @IBAction func fetchData(_ sender: UIButton?) {
        self.page = 1
        self.viewModel.postsViewModel = []
    }
    
    //Open add new post view controller
    @IBAction func addNewPost(_ sender: UIButton) {
        let addNewPost = storyboard?.instantiateViewController(withIdentifier: "AddPostVC") as! AddPostVC
        //Listen to new post add it posts list and reload collection view
        addNewPost.selectedCharacter
        .subscribe(onNext: { [weak self] post in
            if (post.description != nil && post.title != nil) {
                self!.viewModel.postsViewModel.append(PostViewModel(post: post))
                self!.viewModel.sortPosts()
                self!.postsCollection.reloadData()
            }
        }).disposed(by: disposeBag)
        navigationController?.pushViewController(addNewPost, animated: true)
    }
    
}

extension PostsController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.postsViewModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PostCellIdentifier, for: indexPath) as! PostCell
        let objItem = self.viewModel.postViewModel(at: indexPath.row)
        cell.cellConfigure(item: objItem)
        cell.postLikesBtn.tag = indexPath.row
        
        //Listen to button click to show like and dislike on post card
        cell.postLikesBtn.rx.tap.asDriver()
            .drive(onNext: { [weak self] in
                var postViewModel = self!.viewModel.postViewModel(at: cell.postLikesBtn.tag)
                if (cell.postLikesBtn.isSelected) {
                    postViewModel.post.likes_count = postViewModel.likes_count + 1
                    postViewModel.post.selected = true
                } else {
                    postViewModel.post.likes_count = postViewModel.likes_count - 1
                    postViewModel.post.selected = false
                }
                self!.viewModel.saveViewModel(item: postViewModel, at: cell.postLikesBtn.tag)
        }).disposed(by: cell.bag)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width , height: self.view.frame.height / 2.8)
    }
    
}

//befor showing last 2 cells we call fetch posts api to get new posts
extension PostsController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { (indexPath) in
            if (indexPath.row == self.viewModel.postsViewModel.count - 3 ) {
                self.page += 1
            }
        }
    }
}
