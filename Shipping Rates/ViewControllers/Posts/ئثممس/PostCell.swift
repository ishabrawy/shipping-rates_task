//
//  PostCell.swift
//  Shipping Rates
//
//  Created by Ismael AlShabrawy on 9/14/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import UIKit
import Kingfisher
import RxRelay
import RxSwift

class PostCell: UICollectionViewCell {
    
    private var selectedPostModelVar = BehaviorRelay<PostModel>(value: PostModel())
    var selectedPostModel: Observable<PostModel> {
        return selectedPostModelVar.asObservable()
    }
    
    var postModel: PostModel!
    
    var bag = DisposeBag()
    
    @IBOutlet weak var postTitleLbl: UILabel!
    @IBOutlet weak var postDescriptionLbl: UILabel!
    @IBOutlet weak var postLikesBtn: UIButton!
    @IBOutlet weak var postImageView: UIImageView!
    
    func cellConfigure (item: PostViewModel) {
        self.postModel = item.post
        self.postTitleLbl.text = item.post.title
        self.postDescriptionLbl.text = item.post.description
        self.postLikesBtn.setTitle("  \(item.post.likes_count ?? 0)", for: .normal)
        self.postLikesBtn.setImage(#imageLiteral(resourceName: "ic_like"), for: .selected)
        self.postLikesBtn.setImage(#imageLiteral(resourceName: "ic_dislike"), for: .normal)
        let url = URL(string: item.post.img_url ?? "")!
        
        postImageView.kf.indicatorType = .activity
        let processor = DownsamplingImageProcessor(size: postImageView.bounds.size)
            |> RoundCornerImageProcessor(cornerRadius: 8)
        
        postImageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        postImageView.layer.cornerRadius = 8
        
        postImageView.kf.setImage(
            with: url,
            placeholder: nil,
            options: [.transition(.fade(1)),
                      .loadDiskFileSynchronously,
                      .processor(processor),
                      .scaleFactor(UIScreen.main.scale),
                      .transition(.fade(2)),
                      .cacheOriginalImage],
            progressBlock: { receivedSize, totalSize in
                
        },
            completionHandler: { result in
                self.postImageView.contentMode = .scaleAspectFill
        })
        
        self.postLikesBtn.isSelected = self.postModel.selected ?? false
        
    }
    
    @IBAction func likeBtn(_ sender: UIButton) {
        self.postLikesBtn.isSelected = !(self.postModel.selected ?? false)
    }
    
    override func prepareForReuse() {
        bag = DisposeBag()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
