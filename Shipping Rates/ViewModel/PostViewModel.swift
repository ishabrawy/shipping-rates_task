//
//  PostViewModel.swift
//  Shipping Rates
//
//  Created by Ismael AlShabrawy on 9/14/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import Foundation

struct PostViewModel {
    
    var post: PostModel
    
}

extension PostViewModel {
    
    var id: String {
        return self.post.id ?? ""
    }
    
    var title: String {
        return self.post.title ?? ""
    }
    
    var description: String {
        return self.post.description ?? ""
    }
    
    var img_url: String {
        return self.post.img_url ?? ""
    }
    
    var video_url: String {
        return self.post.video_url ?? ""
    }
    
    var likes_count: Int {
        return self.post.likes_count ?? 0
    }
    
    var selected: Bool {
        return self.post.selected ?? false
    }
    
    
}

