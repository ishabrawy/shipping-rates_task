//
//  RootViewModel.swift
//  Shipping Rates
//
//  Created by Ismael AlShabrawy on 9/14/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay
import Alamofire

class RootViewModel {
    
    let postTitlePublishSubject = PublishSubject<String>()
    let postDescriptionPublishSubject = PublishSubject<String>()
    
    private var postsModelVar = BehaviorRelay<[PostViewModel]?>(value: [])
    var postsModel: Observable<[PostViewModel]?> {
        return postsModelVar.asObservable()
    }
    
    private var imagesModelVar = BehaviorRelay<[ImagesModel]>(value: [])
    var imagesModel: Observable<[ImagesModel]> {
        return imagesModelVar.asObservable()
    }
    
    var postsViewModel: [PostViewModel]
    
    
    init() {
        self.postsViewModel = [PostViewModel]()
    }
    
    func fetchData(page: Int) {
        let resource = Resource<[PostModel]>(url: URL(string: Utils().parseConfig().BaseURL)!, endPoint: "posts")
        let prams = ["page":page,
                     "limit":10]
        Webservice().load(resource: resource, method: .get, prams: prams, headers: nil, encoding: .queryString) { result in
            switch result {
            case .success(let posts):
                if (posts != nil && posts.count > 0) {
                    self.postsViewModel += posts.map(PostViewModel.init)
                    self.sortPosts()
                    self.postsModelVar.accept(self.postsViewModel)
                } else {
                    self.postsModelVar.accept(nil)
                }
                break
            case .failure(_):
                break
            }
        }
    }
    
    func fetchImages() {
        let resource = Resource<[ImagesModel]>(url: URL(string: "https://picsum.photos/v2/")!, endPoint: "list")
        let prams = ["page":getRandomID(fromNumber: 1, toNumber: 20)]
        Webservice().load(resource: resource, method: .get, prams: prams, headers: nil, encoding: .queryString) { result in
            switch result {
            case .success(let images):
                if (images.count > 0) {
                    print(images)
                    self.imagesModelVar.accept(images)
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    func sortPosts() {
        self.postsViewModel.sort {
            Int($0.post.id ?? "1")! > Int($1.post.id ?? "1")!
        }
    }
    
    func isValid() -> Observable<Bool> {
        return Observable.combineLatest(postTitlePublishSubject.asObserver().startWith(""),
                                        postDescriptionPublishSubject.asObserver().startWith("")).map { title, description in
                                            return title.count > 3 && description.count > 5
        }.startWith(false)
    }
    
    func getRandomID(fromNumber: Int, toNumber: Int) -> Int {
        return (Int(arc4random()) % (toNumber-fromNumber)) + fromNumber
    }
}

extension RootViewModel {
    func postViewModel(at index: Int) -> PostViewModel {
        return self.postsViewModel[index]
    }
    
    func saveViewModel(item postViewMode: PostViewModel, at index: Int) {
        self.postsViewModel[index] = postViewMode
        self.postsModelVar.accept(self.postsViewModel)
    }
}
