//
//  ImagesModel.swift
//  Shipping Rates
//
//  Created by Ismael AlShabrawy on 9/16/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import Foundation

struct ImagesModel : Codable {
    let id : String?
    let url : String?
    let download_url : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case url = "url"
        case download_url = "download_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        download_url = try values.decodeIfPresent(String.self, forKey: .download_url)
    }

}
