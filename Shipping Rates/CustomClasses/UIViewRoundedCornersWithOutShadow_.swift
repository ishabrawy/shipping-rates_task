//
//  UIViewRoundedCornersWithOutShadow_.swift
//  Shipping Rates
//
//  Created by Ismael AlShabrawy on 9/14/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import UIKit

@IBDesignable
class UIViewRoundedCornersWithOutShadow_: UIView {
    
    @IBInspectable var backcolor: UIColor = UIColor.white {
        didSet{
            self.backgroundColor = backcolor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 4 {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    override func awakeFromNib() {
        self.setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupView()
    }
    
    func setupView() {
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        self.layer.masksToBounds = false
    }
}
