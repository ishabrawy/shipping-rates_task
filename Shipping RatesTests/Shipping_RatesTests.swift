//
//  Shipping_RatesTests.swift
//  Shipping RatesTests
//
//  Created by Ismael AlShabrawy on 9/14/20.
//  Copyright © 2020 Ismael AlShabrawy. All rights reserved.
//

import XCTest
@testable import Shipping_Rates
import RxSwift
import RxRelay

class Shipping_RatesTests: XCTestCase {
    
    private var selectedChracterVariable = BehaviorRelay<PostModel>(value: PostModel())
    var selectedCharacter: Observable<PostModel> {
        return selectedChracterVariable.asObservable()
    }
    
    var viewModel = RootViewModel()
    let disposeBag = DisposeBag()
    
    //This Will Success
    func testAddNewPost() {
        var postModel = PostModel()
        postModel.title = "Test New Post"
        postModel.description = "Test New Post"
        postModel.likes_count = 0
        postModel.selected = false
        postModel.video_url = ""
        postModel.img_url = ""
        postModel.id = "120"
        
        selectedChracterVariable.accept(postModel)
        XCTAssert(true)
        
    }
    
    //This Will Success
    func testCheckItemsSuccess() {
        viewModel.fetchData(page: 2)
        viewModel.postsModel
            .subscribe(onNext: { posts in
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    if (posts == nil) {
                        XCTAssert(false)
                    } else {
                        if (posts!.count >= 0) {
                            XCTAssertEqual(posts!.count, 10, "all posts must be 10 for every page")
                        } else {
                            XCTAssert(false)
                        }
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    //This Will Fail
    func testCheckItemsFail() {
        viewModel.fetchData(page: 1)
        viewModel.postsModel
            .subscribe(onNext: { posts in
                if (posts == nil) {
                    XCTAssert(false)
                } else {
                    if (posts!.count >= 0) {
                        XCTAssertEqual(posts!.count,  10, "all posts must be 0")
                    } else {
                        XCTAssert(false)
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    
    
}
